const socket = io.connect('http://localhost:3000');

socket.on('carrie', (txt) => {
    alert(txt);
});

document.getElementById("sendButton").onclick = () => {
    const text = document.getElementById("textValue").value;
    socket.emit('text sent', text);
};