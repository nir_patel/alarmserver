const express = require('express');
const socket = require('socket.io');

const app = express();
const server = app.listen(3000, () => {
    console.log('listening on port 3000');
});

app.use(express.static('view'));

const alarmList = [];

const io = socket(server);

io.on('connection', (socket) => {
    console.log('socket connected');
    socket.on('disconnect', () => {
        console.log('socket disconnected');
    });
    socket.on('add to-server', (alarm) => {
        console.log('add to-server event emitted');
        const alarmToAdd = JSON.parse(alarm);
        alarmList.push(alarmToAdd);
        console.log(alarmList);
        socket.broadcast.emit('add to-client', alarm);
    });
    socket.on('update to-server', (alarm) => {
        console.log('update to-server event emiited');
        const updatedAlarm = JSON.parse(alarm);
        let found = false;
        for (let i = 0; (i < alarmList.length) && (!found); i++) {
            if (alarmList[i].alarmId === updatedAlarm.alarmId) {
                console.log("update - found a match");
                alarmList[i].name = updatedAlarm.name;
                alarmList[i].description = updatedAlarm.description;
                alarmList[i].date = updatedAlarm.date;
                found = true;
                console.log(alarmList);
                socket.broadcast.emit('update to-client', alarm);
            }
        }
    });
    socket.on('remove to-server', (alarm) => {
        console.log('remove to-server event emitted');
        const alarmToRemove = JSON.parse(alarm);
        let found = false;
        for (let i = 0; (i < alarmList.length) && (!found); i++) {
            if (alarmList[i].alarmId === alarmToRemove.alarmId) {
                console.log("remove - found a match")
                alarmList.splice(i, 1);
                found = true;
                console.log(alarmList);
                socket.broadcast.emit('remove to-client', alarm);
            }
        }
    });
    socket.on('get all from-server', () => {
        console.log('get all from-server event emitted');
        console.log(alarmList);
        socket.emit('send all to-client', JSON.stringify(alarmList).toString());
    });
});
